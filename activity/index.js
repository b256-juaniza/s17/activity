console.log("Hello World");
console.log("");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function userID() {
		let userName = prompt("Enter your Full Name: ");
		let userAge = prompt("Enter your Age: ");
		let userLocation = prompt("Enter your Location: ");
		alert("Thank you, "+ userName + " and Welcome to my Page.");

		console.log("Hello, " + userName);
		console.log("You are " + userAge + " years old.");
		console.log("You live in  " + userLocation);
	}

	userID();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favMusicArtist() {
		console.log("My Favorite Artist/Bands");
		console.log("1. Radwimps");
		console.log("2. Mosawo");
		console.log("3. Alekun");
		console.log("4. LiSA");
		console.log("5. Mrs. GREEN APPLE");
	}
	favMusicArtist();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favMovies(){
		console.log("My Favorite Movies");
		console.log("1. Parasyte: PART 1");
		console.log("Rotten Tomatoes Rating: 72%");
		console.log("2. Attack on Titan");
		console.log("Rotten Tomatoes Rating: 34%");
		console.log("3. How to train your Dragon: The Hidden World");
		console.log("Rotten Tomatoes Rating: 87%");
		console.log("4. Kingsman: The Golden Circle");
		console.log("Rotten Tomatoes Rating: 64%");
		console.log("5. Project Power");
		console.log("Rotten Tomatoes Rating: 46%");
	}
	favMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

printUsers();
/*let printFriends() =*/ function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

// printUsers();

/*console.log(friend1);
console.log(friend2);*/